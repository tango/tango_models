---
cwlVersion: v1.2
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}  
  InlineJavascriptRequirement: {}

inputs:
  pure_culture:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]
          freud_sim: string
  
outputs:
  result_optimization:
    type: File
    outputSource: combine/result_optimization

doc: |-
  For each model file in the input model_list, run an individual
  optimization to obtain lambda, k_lactate, vmax_lactose parameters
  as each model requires. Return a combined result_optimization.

steps:
  
  optimize_each:
    run: tango_models.cwl
    scatter: [model, freud_sim]
    scatterMethod: dotproduct
    in:
      model:
        source: pure_culture
        valueFrom: $([self.models[0]])  # wrap model in singleton array
      freud_sim:
        source: pure_culture
        valueFrom: $(self.freud_sim)
      optimize:
        valueFrom: "True"
      community_scale: 
        valueFrom: "False"
    out: [result_optimization]
    
  combine:
    in:
      file_list:
        source: optimize_each/result_optimization
    out: [result_optimization]
    run:
      class: CommandLineTool
      baseCommand: ["cat"]
      inputs:
        file_list:
          type: File[]
          inputBinding:
            position: 1
      outputs:
        result_optimization:
          type: File
          streamable: true
          outputBinding: {glob: "res_optim.txt"}
      stdout: "res_optim.txt"
