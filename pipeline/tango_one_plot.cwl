cwlVersion: v1.2
class: CommandLineTool

label: Plot TANGO results
doc: |-
  Use plot_* scripts to make figures from TANGO results.

requirements:
  ShellCommandRequirement: {}
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entry: $(inputs.scripts)
        entryname: scripts
      - entry: $(inputs.data)
        entryname: data
      - entry: $(inputs.models)
        entryname: metabolic_models
      - entry: $(inputs.results)
        entryname: results
  NetworkAccess:
    class: NetworkAccess
    networkAccess: true

inputs:
  data:
    type: Directory
    doc: |-
      Directory containing experimental data, referenced in optimize
      configuration
    default:
      class: Directory
      location: ../data

  figure:
    doc: |-
      Which predefined figure to make
      {indiv,flux,com,goodness_of_fit,switch_pathways}
    type: string
    default: "indiv"

  models:
    type: Directory?
    doc: |-
      Directory containing experimental data, referenced in optimize
      configuration

  results:
    # type: File[]
    type: Directory
    doc: |-
      Array of TANGO result files

  scripts:
    type: Directory
    doc: |-
      Scripts for predefined figures
    default:
      class: Directory
      location: ../scripts

baseCommand:
  - python
  - "-m"

arguments:
  - valueFrom: $("scripts.plot_" + inputs.figure)

outputs:
  plots:
    type:
      type: array
      items: File
    outputBinding:
      glob:
        - "*.pdf"
        - "*.svg"
    doc: |-
      Figure in PDF format
    format: edam:format_3508
  standard_output:
    type: stdout
    format: edam:format_1964
  standard_error:
    type: stderr
    format: edam:format_1964

stdout: stdout.txt
stderr: stderr.txt
 
s:author:
  - class: s:Person
    s:name: Simon Labarthe
    s:identifier: https://orcid.org/0000-0003-2114-0697
  - class: s:Person
    s:name: Clémence Frioux
    s:identifier: https://orcid.org/0000-0003-2114-0697
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005
  - class: s:Person
    s:name: Maxime Lecomte
    s:identifier: https://orcid.org/0000-0002-4558-6151
  - class: s:Person
    s:name: Hélène Falentin
    s:identifier: https://orcid.org/0000-0001-6254-5303
  - class: s:Person
    s:name: Julie Aubert
    s:identifier: https://orcid.org/0000-0001-5203-5748
 
s:dateCreated: "2024-03-04"
s:license: https://spdx.org/licenses/LGPL-3.0-or-later
 
$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.23.owl
