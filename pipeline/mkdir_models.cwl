cwlVersion: v1.2
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}  
  InlineJavascriptRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  pure_culture:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]
          freud_sim: string
  community:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]
  
outputs:
  models_dir:
    type: Directory
    outputSource: mkdir_models/out_dir

# This seems needlessly complicated: scattering over experiments,
# boxing the models in an array, flattening the array.

# For correctness we must consider pure_culture and community, but
# models will typically be in both; rely on mkdir_models to avoid
# duplication.

steps:
  flatten_models:
    scatter: nested
    scatterMethod: dotproduct
    in:
      nested:
        source: [pure_culture, community]
        valueFrom: $([self.models])
        linkMerge: merge_flattened
    out: [flattened]
    run: flatten_array.cwl

  combine:
    in:
      nested:
        source: [flatten_models/flattened]
    out: [flattened]
    run: flatten_array.cwl

  mkdir_models:
    in:
      file_list:
        source: combine/flattened
      name:
        valueFrom: "models_dir"
    out: [out_dir]
    run: mkdir_files.cwl
