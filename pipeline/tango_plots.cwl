cwlVersion: v1.2
class: Workflow

label: Plot TANGO results
doc: |-
  Assemble all figures requested by which_figures.

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}  
  InlineJavascriptRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  results:
    type:
      - File[]
      - Directory
  which_figures: string[]
  models: Directory
  
outputs:
  figures:
    type: File[]
    outputSource: flatten/flattened 

steps:
  prepare:
    in:
      file_list:
        source: results
      name:
        valueFrom: "results"
    out: [out_dir]
    run: mkdir_files.cwl

  plot_figures:
    scatter: [figure]
    in:
      results:
        source: prepare/out_dir
      figure:
        source: which_figures
      models:
        source: models
    out: [plots]
    run: tango_one_plot.cwl

  flatten:
    in:
      nested:
        source: plot_figures/plots
    out: [flattened]
    run: flatten_array.cwl
