---
cwlVersion: v1.2
class: ExpressionTool

doc: |
  Flatten a nested array of 'Any' type into an array.

requirements:
  InlineJavascriptRequirement: {}

inputs:
  nested:
    type:
      type: array
      items:
        type: array
        items: ["null", Any]

outputs:
  flattened:
    type:
      type: array
      items: Any

expression: |
  ${
    var flattened = [];
    for (var i = 0; i < inputs.nested.length; i++) {
      for (var j = 0; j < inputs.nested[i].length; j++) {
        if (inputs.nested[i][j] != null) {
          flattened.push(inputs.nested[i][j]);
        }
      }
    }
    return {"flattened": flattened};
  }
