cwlVersion: v1.2
class: Workflow

label: Numerical reconciliation of bacterial fermentation in cheese production
doc: |-
  Complete workflow for TANGO as reported in Lecomte et al (2024),
  "Revealing the dynamics and mechanisms of bacterial interactions in
  cheese production with metabolic modelling", Metabolic Eng. 83:24-38
  https://doi.org/10.1016/j.ymben.2024.02.014

  1. Parameters for individual models are obtained by optimization
  2. Individual dynamics and community dynamics are simulated
  3. Figures for the manuscript are assembled from the results.

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}  
  InlineJavascriptRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  # Choose experiments to include in simulations
  pure_culture:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]
          freud_sim: string
  community:
    type:
      type: array
      items:
        type: record
        fields:
          models: File[]

  # Choose plots to make from simulation results
  which_figures: string[]
  
outputs:
  results:
    type: Directory
    outputSource: mkdir_results/out_dir
  figures:
    type: Directory
    outputSource: mkdir_figures/out_dir

steps:      
  optimize:
    in:
      pure_culture:
        source: pure_culture
    out: [result_optimization]
    run: pipeline/tango_optimization_wf.cwl
    
  dynamics:
    in:
      initial_res_optim:
        source: optimize/result_optimization
      pure_culture:
        source: pure_culture
      community:
        source: community
    out: [results, time_series]
    run: pipeline/tango_dynamics_wf.cwl

  mkdir_results:
    in:
      file_list:
        source: dynamics/results
      name:
        valueFrom: "results"
    out: [out_dir]
    run: pipeline/mkdir_files.cwl

  mkdir_models:
    in:
      pure_culture:
        source: pure_culture
      community:
        source: community
    out: [models_dir]
    run: pipeline/mkdir_models.cwl

  plot_figures:
    in:
      results:
        source: [dynamics/results, dynamics/time_series]
        linkMerge: merge_flattened
      which_figures:
        source: which_figures
      models:
        source: mkdir_models/models_dir
    out: [figures]
    run: pipeline/tango_plots.cwl

  mkdir_figures:
    in:
      file_list:
        source: plot_figures/figures
      name:
        valueFrom: "figures"
    out: [out_dir]
    run: pipeline/mkdir_files.cwl

s:author:
  - class: s:Person
    s:name: Simon Labarthe
    s:identifier: https://orcid.org/0000-0003-2114-0697
  - class: s:Person
    s:name: Clémence Frioux
    s:identifier: https://orcid.org/0000-0003-2114-0697
  - class: s:Person
    s:name: David James Sherman
    s:identifier: https://orcid.org/0000-0002-2316-1005
  - class: s:Person
    s:name: Maxime Lecomte
    s:identifier: https://orcid.org/0000-0002-4558-6151
  - class: s:Person
    s:name: Hélène Falentin
    s:identifier: https://orcid.org/0000-0001-6254-5303
  - class: s:Person
    s:name: Julie Aubert
    s:identifier: https://orcid.org/0000-0001-5203-5748

s:citation: https://doi.org/10.1016/j.ymben.2024.02.014
s:codeRepository: https://forgemia.inra.fr/tango/tango_models.git
s:license: https://spdx.org/licenses/LGPL-3.0-or-later
s:programmingLanguage: Python
s:dateCreated: "2024-03-02"

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
 - http://edamontology.org/EDAM_1.23.owl
