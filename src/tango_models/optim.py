import math
import time
from decimal import *

import cobra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy as scp
from scipy.integrate import odeint
from scipy.optimize import Bounds, minimize

from . import customize, dFBA, preprocessing_data


def model_func_dynamic(theta,data,param,model):
    """[summary]
    Computes a dFBA and extract the solution at times data['times']

    Args:
        theta (array): parameters being optimized
        data (pd.DataFrame): array of data time points: first column = times, second column= bacterial density
        param (dict): dictionnary of all essential parameters used in the dFBA and optim program
        model (Model): current model used for optimization

    Returns:
        dict: system solution at observation times
    """

    dfba_evaluated_at_times ={}

    times=data['time']
    for i_theta, t in enumerate(theta):
        param[param['dictionnary_theta_param_keys'][model.id][i_theta]] = {model.id:t}
    customize.ComputeProductionBoundsFromData(param,[model])
    dfba_evaluated_at_times[model.id] = dFBA.DFBA(param,times=times,b=model.id,lactic_acid = param['lactic_acid_model'])
        
    return dfba_evaluated_at_times


def observation_func_dynamic(theta,data,param,model,weight,mu):
    """ cost function

    Args:
        theta (array): parameters being optimized
        data (pd.DataFrame): array of data time points: first column = times, second column= bacterial density
        param (dict): dictionnary of all essential parameters used in the dFBA and optim program
        model (Model): current model used for optimization

    Returns:
        ndarray: Distance between simulated data and experimental data
    """    
    # Computing model predictions
    # NB: Overriding of initial conditions from data
    dfba_prediction={}
    for key in data.keys():
        if key=='ExpGrowth':
            param['y0'][param['Y_id'][model.id]]=10**data['ExpGrowth']['mean'][data['ExpGrowth']['mean']['time']==0]['numbering']  
#            param["y0"][param['Y_id']["EX_lac__D_e"]]=dFBA.convert_g_kg_in_mmol_g(param['lactate_concentration'][model.id],molar_mass=param['molar_mass']["EX_lac__L_e"])
#            param["y0"][param['Y_id']["EX_lac__L_e"]]=dFBA.convert_g_kg_in_mmol_g(param['lactate_concentration'][model.id],molar_mass=param['molar_mass']["EX_lac__L_e"])
        elif key=='ExpMetabolites':
            try:
                 param['y0'][param['Y_id'][model.id]]=10**data['ExpGrowth']['mean'][data['ExpGrowth']['mean']['time']==0]['numbering'] #same bacterial inoculum than for bacterial growth
            except:
                 param['y0'][param['Y_id'][model.id]]=10**data[key]['mean'][data[key]['mean']['time']==0][model.id] #same bacterial inoculum than for bacterial growth

            param["y0"][param['Y_id']["EX_lac__D_e"]]=data[key]['mean'][data[key]['mean']['time']==0]['EX_lac__L_e']/2 # data already converted (see GetData function). Divided by 2 because dosage of the whole lactate.
            param["y0"][param['Y_id']["EX_lac__L_e"]]=data[key]['mean'][data[key]['mean']['time']==0]['EX_lac__L_e']/2 # data already converted (see GetData function). Divided by 2 because dosage of the whole lactate.
        else:
            raise NameError('experiment name is not expected: must be ExpGrowth or ExpMetabolites')           
        
        model_prediction = model_func_dynamic(theta,data[key]['mean'],param,model)[model.id]
        

        #output extraction in pandas dataframe
        output_mapping = {k:k for key in data.keys() for k in data[key]['mean'].columns if k not in ['numbering']}
        output_mapping['numbering']=model.id
        output_mapping['pH']='ph'
        dfba_prediction[key] = pd.DataFrame({output_field:model_prediction[:,param['Y_output_id'][output_mapping[output_field]]] for output_field in data[key]['mean'].columns}).set_index('time',drop=False)
        if 'EX_lac__L_e' in dfba_prediction[key]:
            dfba_prediction[key]['EX_lac__L_e']+=model_prediction[:,param['Y_output_id']['EX_lac__D_e']]
        #dfba_prediction[key].index=data[key].index
    PLOT=True
    if PLOT:
        #Growth
        dFBA.PlotDFBAResult(param,model,dfba_prediction=dfba_prediction,data=data)


    ######### distance output
    epsilon=1e-9
    cost=0.0
    curr_dist_ph=0.0
    curr_dist_growth=0.0
    for key in data.keys():
        curr_dist=0.0
        if key=='ExpGrowth':    
            max_std=np.min(data[key]['std']['numbering'][data[key]['std']['numbering']>0])
            #log normal distance for bacterial growth
            curr_dist_growth=np.linalg.norm((data[key]['mean']['numbering']-np.log10(dfba_prediction[key]['numbering']+epsilon))/(np.maximum(data[key]['std']['numbering'],max_std)))**2
            curr_dist+=curr_dist_growth
            #weighted normal distance for ph
            if weight['pH']>0.0:
                max_std=np.min(data[key]['std']['pH'][data[key]['std']['pH']>0])
                curr_dist_ph=weight['pH']*np.linalg.norm((data[key]['mean']['pH']-dfba_prediction[key]['pH'])/(np.maximum(data[key]['std']['pH'],max_std)))**2
                curr_dist+=curr_dist_ph
        elif key=='ExpMetabolites':        
            curr_data=data[key]['mean'].drop('time',axis=1).values
            curr_weight = np.ones((curr_data.shape[1],))
            curr_data_masked = np.ma.array(curr_data,mask=np.isnan(curr_data))
            curr_simulation_masked = np.ma.array(dfba_prediction[key].drop('time',axis=1).values,mask=np.isnan(curr_data))
            curr_std = data[key]['std'].drop('time',axis=1).values
            curr_std_masked = np.ma.array(curr_std,mask=np.isnan(curr_data))
            if model.id in data[key]['mean'].drop('time',axis=1).columns:
                i_mod = data[key]['mean'].drop('time',axis=1).columns.get_loc(model.id)
                #data are already log-transformed
                curr_data_masked[:,i_mod]=curr_data_masked[:,i_mod]
                curr_simulation_masked[:,i_mod]=np.log10(curr_simulation_masked[:,i_mod])
                curr_weight[i_mod] = weight['growth']
            curr_dist+=np.sum(curr_weight*np.sum(np.square(curr_data_masked-curr_simulation_masked)/(np.square(curr_std_masked)+epsilon),axis=0))
        cost+=mu[key]*curr_dist    
    print('current cost function',cost,'growth',curr_dist_growth,"ph",curr_dist_ph)
    print('current parameters',' '.join([param['dictionnary_theta_param_keys'][model.id][i_theta]+":"+str(i) for i_theta,i in enumerate(theta)]))
    with open('log_'+model.id+'_tmp.txt','a') as f:
        f.write('current cost function '+str(cost)+', growth '+str(curr_dist_growth)+", ph "+str(curr_dist_ph)+'\n')
        f.write('current parameters' + ' '.join([param['dictionnary_theta_param_keys'][model.id][i_theta]+":"+str(i) for i_theta,i in enumerate(theta)])+'\n')
    return cost

def getData(model,data_key,param,keys=['mean','std']):
    """ get experimental data

    Args:
        model (Model): current model used for optimization
        data_key (str): name of the dataset (ExpGrowth, or ExpMetabolites)
        param (dict): parameters

    Returns:
        pd.DataFrame: data of the current species
    """    
    # Data is a dataframe with a first column indicating the time of sampling and a second column indicating the bacterial density.
    data={}
    model_data=pd.read_csv(param['data_optim'][model.id][data_key]['path'],sep=',')
    if data_key=='ExpMetabolites':
        for key in keys:
        #reformatting data to get the metabolites by columns        
            M= model_data[['time','metabolite',key]].pivot_table(index=['time'],columns=['metabolite'])
            M.columns=M.columns.droplevel(0)
        # data conversion, from g.kg^-1 to mmol.g^-1
            M = M.apply(lambda x: dFBA.convert_g_kg_in_mmol_g(x,molar_mass=param['molar_mass'][x.name]) if x.name not in ['time'] else x, axis=0)
            M = M.reset_index().set_index('time',drop=False)
            data[key]=M
        #model_data['species']='freudenreichii'
    else:        
        A_pivot=model_data.pivot_table(index=['species','time'],columns=['type','replicate']).droplevel(0,axis=1)
        for val in A_pivot.columns.levels[0]:
            vals=A_pivot[val].columns
            convert_cfu_g_to_g_g=0.33*1e-12
            if val=='numbering':
                for vals2 in vals:
                    ## log transform for growth data
                    A_pivot.loc[:,(val,vals2)]=np.log10(A_pivot[val][vals2]*convert_cfu_g_to_g_g)
            A_pivot[(val,'mean')] = A_pivot[val][vals].mean(axis=1,skipna=True)
            A_pivot[(val,'std')] = A_pivot[val][vals].std(axis=1,skipna=True)
        extract=[(val1,val2) for val2 in ['mean','std'] for val1 in A_pivot.columns.levels[0]]
        A_final=A_pivot[extract].swaplevel(0,1,axis=1)
        for key in keys:
            if model.id in ['lactis','plantarum']:
                data[key]=A_final.loc[model.id,key].reset_index().set_index('time',drop=False).dropna()
            else:
                data[key]=A_final.loc[model.id,key].reset_index().set_index('time',drop=False)[['time','numbering']]
    return data

def getStarted(objective_func, theta,bounds):
    """[summary]

    Args:
        objective_func ([type]): [description]
        theta ([type]): [description]
        bounds ([type]): [description]

    Returns:
        [type]: [description]
    """    
    method ='L-BFGS-B' #'Nelder-Mead' #,'maxfev':6000
    
    res = minimize(objective_func, theta, bounds=bounds,method=method,options={'disp':True,'ftol':1.e-9,'eps':1.0e-5})#,'maxls':40}) # 'disp' option fo displaying convergence messages # option eps set the step in parameter space to compute the jacobian with finite difference method. If too small, no difference is captured with the gradient=> gradient is null and algorithm stops.
    return res

def ProfileObjectiveFunction(model,theta_bounds,param,data_paths):
    """Profiles the objective function according to each dimension of the optimization parameter vector.
    
    For the parameter vector theta=(theta_1,...,theta_n), this function display in a graph the function theta_i ----> Objective_function(theta), for initial values of theta_j, j \neq i.

    Args:
        model (Cobra model instance): FBA model
        theta_bounds (tupple of tupples): tupple of bounds on theta_j, for j=1,...,n
        param (dictionnary): dictionnary of DFBA parameters 
        data_paths (string): path to the original data

    Returns:
        None: None
    """


    model_data = getData(model,data_paths)
    theta = param['theta_initial_value'][model.id]
    # theta_bounds=Bounds([0],[1000])
    objective_func_dynamic = lambda theta: observation_func_dynamic(theta,model_data,param,model)
    n_points=5
    initial_value=objective_func_dynamic(theta)
    profile_values=np.zeros((n_points,theta.shape[0]))
    fig,ax=plt.subplots(1,1)
    for j in np.arange(theta.shape[0]):
        print(10*'*')
        theta_curr=theta.copy()
        bound_min = theta[j]*0.5
        bound_max = theta[j]*1.5        
        val_curr = np.linspace(bound_min,bound_max,n_points)
        for i_val,val in enumerate(val_curr):
            theta_curr[j] = val
            profile_values[i_val,j]=objective_func_dynamic(theta_curr)
            print('parameter :',str(j),"parameter value :",val,' objective function value :',profile_values[i_val,j])
        #normalization
        
        
        ax.plot(val_curr/theta[j],profile_values[:,j]/initial_value,c='C'+str(j),label='theta_'+str(j))
    ax.legend()
    fig.savefig('pipeline/profile_objective_function.pdf')

def optimDFBA(model,param, lactic_acid='total'):
    """[summary]

    Args:
        model ([type]): [description]
        theta_bounds ([type]): [description]
        param ([type]): [description]
        data_paths ([type]): [description] 

    Returns:
        [type]: [description]
    """    
    model_data = { k : getData(model,k,param) for k in param['data_optim'][model.id].keys()}
    model_mu = { k : param['data_optim'][model.id][k]['weight_mu'] for k in param['data_optim'][model.id].keys()}    
    theta_init = param['theta_initial_value'][model.id]
    theta_bounds = param['theta_bounds'][model.id]
    weight = {'pH':param['weight_ph_in_observation_function'][model.id],'growth':param['weight_growth_in_observation_function'][model.id]}

    print(5*'*'+' Optimization parameters '+5*'*')
    print(3*'-'+' Data')
    print("model_data",model_data)
    print(3*'-'+' theta to be optimized')
    for t in param['theta_param'][model.id]:
        print(t,"theta_init",theta_init[param['dictionnary_param_keys_theta'][model.id][t]],"theta_bounsd",theta_bounds[param['dictionnary_param_keys_theta'][model.id][t]])
    print(3*'-'+' Optimization weights')
    print("weight",weight)
    print("mu",model_mu)
    print(15*'*')
    param['lactic_acid_model']=lactic_acid
    objective_func_dynamic = lambda theta: observation_func_dynamic(theta,model_data,param,model,weight,model_mu)

    print(param)

    if 'intrinsic_flux_product' in param.keys():
        print("customized fluxes",param['intrinsic_flux_product'])

    res = getStarted(objective_func_dynamic,np.array(theta_init),bounds=theta_bounds)
    if res.success:
        print(res)
    else:
        print('OPTIMISATION FAILED')
        print(res)
    return res

