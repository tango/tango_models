import csv
import os
import pickle
import time

import cobra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
from matplotlib.pyplot import axis, fill
from numpy import core

from . import customize, dFBA, optim, preprocessing_data, simulation

# import newTranscripto_copy
#import automated_petites_phrases
#import Illustration_paper
#import test
# import mt_integration

print('cobra version :',cobra.__version__)

def get_config_from_file(culture_config='pipeline/config_file/config_culture.yml', dynamic_config='pipeline/config_file/config_dynamic.yml', optim_config='pipeline/config_file/config_optim.yml'):

    with open(culture_config,'r') as f:
        try:
            sp_reac = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            raise NameError('Error Reading config file\n'+str(exc))

    with open(dynamic_config,'r') as f:
        try:
            media_reac = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            raise NameError('Error Reading config file\n'+str(exc))

    with open(optim_config,'r') as f:
        try:
            param_sp = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            raise NameError('Error Reading config file\n'+str(exc))      
    return sp_reac,media_reac,param_sp


def CheckAllEquals(model_list,param,data_name):
    #check if the values are all equals for data when several models
    arr_val = np.array([param[data_name][model.id] for model in model_list])
    if not np.all(arr_val==arr_val[0]):
        raise NameError('Warning : the different models have not the same intial conditions for the '+data_name+' in the config file. Please correct.')
    return np.all(arr_val==arr_val[0])

def GetLexicographicOrder():
    dict_to_return = {
                                    'plantarum':{
                                            'reaction_order':['Growth','EX_lcts_e','EX_lac__L_e','EX_lac__D_e','EX_ac_e','EX_btd_RR_e'],
                                            'Direction':["max","min","max","max","max","max"]
                                            },
                                    'lactis':{
                                            'reaction_order':["Growth","EX_diact_e","EX_btd_RR_e","EX_lcts_e","EX_lac__L_e","EX_lac__D_e","EX_cit_e","EX_ac_e"],
                                            'Direction':["max","max","max","min","max","max","min","max"]
                                            },
                                    'freudenreichii':{
                                            'reaction_order':["Growth","EX_lac__L_e","EX_lac__D_e","EX_lcts_e","EX_ac_e","EX_ppa_e",'EX_pyr_e','EX_succ_e',"EX_biomass_e"],
                                            'Direction':["max","min","min","min","max","max",'max','max','max']
                                            }
    }
    return dict_to_return

def define_param(model_list,media_reac,param_sp, optim_c=False, com_c=False,limit_dt=1.0, K_ph_regulation = 10, Parsimonious=False, Lexicographic=True, Lexicographic_Secured_Mode=False, lactic_acid=None):
    optim=optim_c=='True'
    com=com_c=='True'
    theta_weight_in_observation_function=optim
    param={}
    #list of bacteria and compounds included in the system dynamics
    param['bact']=[model.id for model in model_list]
    param['Screened_Compounds']={model.id:[model.reactions.get_by_id(r) for r in media_reac[model.id]['interest_compounds'] if r in model.reactions] for model in model_list}
    param['Screened_Compounds_id']=sorted(list(set([el.id for model in model_list for el in param['Screened_Compounds'][model.id]])))
    param['compound_name']=param['bact']+param['Screened_Compounds_id']

    #model dictionnary
    param['model_dict']={model.id:model for model in model_list}

    #output flag (all models have identical flag => take the first one)
    param['output_flag'] = media_reac[model_list[0].id]['output_flag']

    
    #parameters of the system dynamics
    param_list = ['death_rate','lambda','lactate_lower','lactose_lower',
                  'ppa_upper','ac_upper','thresh_qs','ph','lambda_biomass', 'oxygene', 'k_lactate','vmax_lactose','vmin_lactose','c1','c2','lactate_upper','K_lac', 'thresh_qs_pure_culture','thresh_qs_com']
    for p in param_list:
        try: #allow ignoring error when a parameter is not convertible to float in media_reac (e.g. when set to '' in media_reac)
            param[p] = {model.id:float(media_reac[model.id][p]) for model in model_list if p in media_reac[model.id].keys()}
        except:
            print('error for ',p)
            pass
    param['biomass_function_dict']={model.id:media_reac[model.id]['obj_function'] for model in model_list}
    


    param['theta_param']={model.id:[tp for tp in param_sp[model.id]['theta_param'].keys()] for model in model_list}
    param['molar_mass'] = {'EX_lac__L_e':90.08,'EX_lac__D_e':90.08,'EX_ac_e':60.052,'EX_pyr_e':88.06,'EX_succ_e':118.09,'EX_ppa_e':74.08, 'EX_lcts_e': 342.3, 'EX_cit_e':192.124} 
    param['molar_mass']['EX_diact_e']=1.0 #diacetyl is in arbitrary units => no conversion =>molar mass=1
    param['molar_mass']['freudenreichii']=1.0 #no conversion for biomass
    param['concentration_to_export'] = {'lactose_concentration':['EX_lcts_e'],'lactate_concentration':['EX_lac__L_e','EX_lac__D_e'], 'citrate_concentration': ['EX_cit_e'], 
                                        'acetate_concentration':['EX_ac_e'], 'diacetyl_concentration':['EX_diact_e'], 'succinate_concentration':['EX_succ_e']}

    if optim:
#        for p in ['c1','c2']:
#            param[p] = {model.id:float(media_reac[model.id][p]) for model in model_list}            
        param['dictionnary_theta_param_keys'] = {model.id:{i_idx :i for i_idx,i in enumerate(param['theta_param'][model.id])} for model in model_list}
        param['dictionnary_param_keys_theta'] = {model.id:{i :i_idx for i_idx,i in enumerate(param['theta_param'][model.id])} for model in model_list}
        param['theta_initial_value']= {
                                        model.id:np.asarray([param_sp[model.id]['theta_param'][key]['initial_value'] for key in param['theta_param'][model.id]]) 
                                        for model in model_list
                                        }
        param['theta_bounds']= {
                                model.id:np.asarray([param_sp[model.id]['theta_param'][key]['bounds'] for key in param['theta_param'][model.id]]) 
                                        for model in model_list
                                    }
    param['data_optim']= {model.id: param_sp[model.id]['data_optim'] for model in model_list}


    if com:
        param['path_time_data_com']=[param_sp[model.id]['data_path_com'] for model in model_list]
        param['path_DM_data_com']=[param_sp[model.id]['data_DM_path_com'] for model in model_list]
        param['theta_param_com']={model.id:param_sp[model.id]['theta_param_com'] for model in model_list}
        param['dictionnary_theta_param_keys'] = {(model.id if param['theta_param_com'][model.id]!='None' else model.id):({i_idx :i for i_idx,i in param['theta_param_com'][model.id].items()}  if param['theta_param_com'][model.id]!='None' else None ) for model in model_list}
        for p in ['c1_com','c2_com','k_lactate_com','vmax_lactose_com','vmin_lactose_com']:            
            param[p] = {model.id:float(media_reac[model.id][p]) for model in model_list if p in media_reac[model.id].keys()}
    
        
    if theta_weight_in_observation_function:
        param['weight_ph_in_observation_function']={model.id:float(param_sp[model.id]['weight_pH_in_observation_function']) for model in model_list if 'weight_pH_in_observation_function' in param_sp[model.id].keys()}
        param['weight_growth_in_observation_function']={model.id:float(param_sp[model.id]['weight_growth_in_observation_function']) for model in model_list if 'weight_growth_in_observation_function' in param_sp[model.id].keys()}
        
    param['compound_name']+=['ph']
    param['Y_id']={func:func_id for func_id,func in enumerate(param['compound_name'])}
    param['Y_id_array_without_ph']=np.array([param['Y_id'][func] for func in param['compound_name'] if func != 'ph'])
    param['Y_output_id']={func:func_id for func_id,func in enumerate(['time']+param['compound_name'])}
    param['Y_output_id_inv']={func_id:func for func,func_id in param['Y_output_id'].items()}
#    param['Screened_Compounds']={model.id:[model.reactions.get_by_id(r) for r in media_reac[model.id]['interest_compounds'] if r in model.reactions] for model in model_list}    
    param['bact_Y_id_by_screened_compound']={
                                              compound_id: np.array(
                                                        [param['Y_id'][b] for b in param['bact'] if compound_id in [r.id for r in param['Screened_Compounds'][b]]]
                                                        ) 
                                              for compound_id in param['Screened_Compounds_id']}
    id_bact=np.array([param['Y_id'][b] for b in param['bact']])
    id_subs=np.array([param['Y_id'][b] for b in param['Screened_Compounds_id']])
    id_ph=np.array([param['Y_id'][b] for b in param['compound_name'] if 'ph' in b])

    concentration_list = ['lactose_concentration', 'lactate_concentration', 'citrate_concentration', 'acetate_concentration', 'diacetyl_concentration','succinate_concentration']
    for p in concentration_list:
        try:
            param[p]={model.id:float(media_reac[model.id][p]) if media_reac[model.id][p] != '' else '' for model in model_list}
        except:
            pass
        #check if the values are all equals when several models
        CheckAllEquals(model_list,param,p)
        
    #check if the values are all equals for ph when several models
    param['intrinsic_flux']={model.id:{met.id:float(media_reac[model.id]['intrinsic_flux']) for met in param['Screened_Compounds'][model.id]} for model in model_list}
    for model in model_list:
        for k in media_reac[model.id].keys():
            if 'intrinsic_flux_' in k:
                param['intrinsic_flux'][model.id][k.split('intrinsic_flux_')[1]]=float(media_reac[model.id][k])

    ###################################################
    ############### definition of y0 ##################
    y0=np.zeros((len(param['compound_name']),)) #substrates

    ### pH
    #check if the values are all equals for ph when several models
    check=CheckAllEquals(model_list,param,'ph')
    arr_val = np.array([param['ph'][model.id] for model in model_list])
    if check: 
        y0[param['Y_id']['ph']] = param['ph'][model_list[0].id] #

    ## compound concentration
    for cmpd in concentration_list:
        ex_cmpd_l = param['concentration_to_export'][cmpd]
        for ex_cmpd in ex_cmpd_l:
            if ex_cmpd in param['compound_name'] and param[cmpd] != '':
                y0[param['Y_id'][ex_cmpd]] = dFBA.convert_g_kg_in_mmol_g(param[cmpd][model_list[0].id],molar_mass=param['molar_mass'][ex_cmpd])

    # override initial condition with null value
    check=CheckAllEquals(model_list,{'interest_compounds_0':{model.id:media_reac[model.id]['interest_compounds_0'] for model in model_list}},'interest_compounds_0')
    if media_reac[model_list[0].id]['interest_compounds_0'] != [''] :
        for i in media_reac[model_list[0].id]['interest_compounds_0']:
            y0[param['Y_id'][i]] = 0.0
    if True:#'lactate_concentration' not in param.keys():
        y0[param['Y_id']['EX_lac__L_e']] =float(media_reac[model_list[0].id]["lactate_concentration"])/2 #dFBA.getLactateConcentrationFromPh(param['ph'][model_list[0].id],c1=param['c1'][model_list[0]], c2 = param['c2'][model_list[0]], lactic_acid=lactic_acid)/2
        y0[param['Y_id']['EX_lac__D_e']] = float(media_reac[model_list[0].id]["lactate_concentration"])/2#dFBA.getLactateConcentrationFromPh(param['ph'][model_list[0].id],c1=param['c1'][model_list[0]], c2 = param['c2'][model_list[0]], lactic_acid=lactic_acid)/2 
    
    
    # initial conditions for bacteria
    cfu_to_g_DW=0.33*10**-12
    for model in model_list:
        if float(media_reac[model.id]['inoculum'])>10: #data in CFU
            y0[[param['Y_id'][model.id]]]=float(media_reac[model.id]['inoculum'])*cfu_to_g_DW
        elif float(media_reac[model.id]['inoculum'])<=10 and float(media_reac[model.id]['inoculum'])>0: # data in log CFU
            y0[[param['Y_id'][model.id]]]=10**float(media_reac[model.id]['inoculum'])*cfu_to_g_DW
    param['y0'] = y0
    param['id_subs'] = id_subs
    param['id_ph'] = id_ph
    param['id_bact'] = id_bact
    #check if the values are all equals for dt when several models
    CheckAllEquals(model_list,{'dt':{model.id:media_reac[model.id]['dt'] for model in model_list}},'dt')
    param['dt'] = float(media_reac[model.id]['dt'])
    CheckAllEquals(model_list,{'Tf':{model.id:media_reac[model.id]['Tf'] for model in model_list}},'Tf')
    param['Tf'] = int(media_reac[model.id]['Tf'])

    param['limit_dt']=limit_dt
    param['K_ph_regulation'] = K_ph_regulation
    param['Parsimonious']= Parsimonious
    param['Lexicographic']= Lexicographic
    # Lexicographic order for the function cobra.util.add_lexicographic_constraints. Optimization will be performed in the order of the lexicographic order, with given direction in the Direction list.
    param['Lexicographic_dict']= GetLexicographicOrder()
    param['Lexicographic_Secured_Mode']=Lexicographic_Secured_Mode


    if 'lactate_upper' not in param.keys():
        param['lactate_upper']={'freudenreichii':None,'plantarum':1000,'lactis':1000}
    if 'acetate_upper' not in param.keys():
        param['acetate_upper']={'freudenreichii':None,'plantarum':None,'lactis':1000}

#    print('y0',param['y0'])
    for i in param['Y_id'].keys():
        print(i,param['y0'][param['Y_id'][i]])


    return param







def sim(sp_reac,media_reac,param_sp,model_list,args_c):

    param=define_param(model_list,media_reac,param_sp,com_c=args_c.com,optim_c=args_c.optim,lactic_acid=args_c.lactic_acid_model)
    customize.block_flux_reaction(param,sp_reac,model_list)
    customize.media_individual_species(param,media_reac,model_list,sp_reac)
    customize.SetO2Bound(param,model_list)    
    customize.ComputeProductionBoundsFromData(param,model_list)


    if args_c.com == 'True':        
        time_dFBA_init=time.time()
  
        if args_c.optim == 'True':
            # to be implemented
            raise NameError('To be implemented')
   
        else: #args_c.optim=='False'                   
            param['interest_value'] = ['Linoc','Moul','Demoul','AS','Aff4s','Aff7s']
            for mod in param['theta_param'].keys():
                print('mod',mod)
                for p in param['theta_param'][mod]:
                    print(p,param[p])
            param=get_theta_param_from_optimFile(param,model_list)
            customize.ComputeProductionBoundsFromData(param,model_list)            
            for mod in param['theta_param'].keys():
                print('mod',mod)
                for p in param['theta_param'][mod]:
                    print(p,param[p])            
            std,times,DM = preprocessing_data.get_experimental_data(param['path_time_data_com'],param['path_DM_data_com'],'data/TANGO_MTA_WF_310320.xlsx', interest_value = param['interest_value'])
            res=dFBA.DFBA(param,times=times,DM=DM,com=True,transcrip=False,opt=None,VERBOSE=args_c.verbose,lactic_acid=args_c.lactic_acid_model)
            print(pd.DataFrame(res))
            time_dFBA_end=time.time()
            print('\n **********************************\n')
            print('Time dFBA:',time_dFBA_end-time_dFBA_init)
            # dFBA.draw(res,param,com=True,opt=True)
            res.to_csv('results/result_dfba_com_without_optim.csv',sep='\t')

    else: #args_c.com=='False'
        if len(model_list)>1:
            print('model_list is',model_list)
            raise NameError('model_list is larger than 1 item => specify only one item to optimize on')
        else:
            model = model_list[0]    
        if args_c.optim == "True":
            for model in model_list:
                param['model_dict'][model.id].solver=args_c.Cobra_Solver #'cplex'
            with open('res_optim.txt','a+') as txt_file:
                time_dFBA_init=time.time()
                print('I am in simulation--> optim')
                print('y0')
                print(param['y0'])
                res_optim=optim.optimDFBA(model,param,lactic_acid=args_c.lactic_acid_model)

                for i in range(len(res_optim.x)):
                    txt_file.write(param['model_dict'][model.id].id+"\t"+param['dictionnary_theta_param_keys'][model.id][i]+"\t"+str(res_optim.x[i])+"\n")
                time_dFBA_end=time.time()
                print('\n **********************************\n')
                print('Time optim:',time_dFBA_end-time_dFBA_init)

            with open('recovery_optim_'+model.id+'.yml','wb') as f:
                pickle.dump({'param':param,'optimal_param':{param['dictionnary_theta_param_keys'][model.id][i]:res_optim.x[i] for i in range(len(res_optim.x))},'optim_output':res_optim,'model':model},f)


        else: #args_c.optim=='False'
            time_dFBA_init=time.time()        
            param=get_theta_param_from_optimFile(param,model_list)
            customize.ComputeProductionBoundsFromData(param,model_list)
            for model in model_list:
                param['model_dict'][model.id].solver= args_c.Cobra_Solver #'cplex'
            if args_c.recovery=='True':
                with open('recovery_optim_'+model.id+'.yml','rb') as f:
                    Temp = pickle.load(f)
                    model_list=[Temp['model']]
                    for model in model_list:
                        param['model_dict'][model.id].solver= args_c.Cobra_Solver #'cplex'
                    param = Temp['param']
                    for key,value in Temp['optimal_param'].items():
                        param[key]=value                    

            if model.id == 'freudenreichii' and args_c.freud_sim == 'metabolites':  
                param["y0"][param['Y_id']["EX_lac__D_e"]]=dFBA.convert_g_kg_in_mmol_g(16.5,molar_mass=90.08)/2
                param["y0"][param['Y_id']["EX_lac__L_e"]]=dFBA.convert_g_kg_in_mmol_g(16.5,molar_mass=90.08)/2
            res_metabolomique=dFBA.DFBA(param,b=param['model_dict'][model.id].id,com=False,VERBOSE=args_c.verbose,lactic_acid=args_c.lactic_acid_model,freud_sim=args_c.freud_sim)
            res_dfba={'ExpGrowth':pd.DataFrame(res_metabolomique)}
            res_dfba['ExpGrowth'].columns = [param['Y_output_id_inv'][i].replace(model.id,'bacterial density') for i in range(res_metabolomique.shape[1])]
            # print(res_dfba)
            if model.id != "freudenreichii":
                res_dfba['ExpGrowth'].to_csv('pipeline/result_pure_culture_'+'_'.join([model.id for model in model_list])+".csv")
            else:
                res_dfba['ExpGrowth'].to_csv('pipeline/result_pure_culture_'+'_'.join([model.id for model in model_list])+'_'+args_c.freud_sim+".csv")
            #dFBA.PlotDFBAResult(param,model,dfba_prediction=res_dfba,data=None,tag='_test_')
            #dFBA.PlotDFBAResult(param,model,dfba_prediction=res_dfba,label='EX_lac__D_e',data=None,tag='_test2_')
            #dFBA.PlotDFBAResult(param,model,dfba_prediction=res_dfba,label='EX_lac__L_e',data=None,tag='_test2_')
            #dFBA.PlotDFBAResult(param,model,dfba_prediction=res_dfba,label='EX_lcts_e',data=None,tag='_test2_')                        
            #prediction_pur_culture(res_metabolomique,predict,param)
    return param            


def get_theta_param_from_optimFile(param,model_list,optim_file='res_optim.txt'):
    print('theta_param',param['theta_param'])
    with open(optim_file) as f:
        lines=[line.strip() for line in f]
        lines=[line.split() for line in lines]
        bact={}
        for b in lines:
            if b[0] not in bact:
                bact[b[0]] = {}
            bact[b[0]][b[1]]=b[2]
        for model in model_list:
            for i_r,r in enumerate(param['theta_param'][model.id]):
                if param['theta_param'][model.id][i_r] in param.keys():
                    param[param['theta_param'][model.id][i_r]][model.id]=float(bact[model.id][r])
                else:
                    param[param['theta_param'][model.id][i_r]] = {}
                    param[param['theta_param'][model.id][i_r]][model.id]=float(bact[model.id][r])
    for b in param['theta_param'].keys():
        print('bact',b)
        for p in param['theta_param'][b]:
            print(p,param[p])
    return param


  
def converted_pur_culture(predict, param):
    for cpd,dicts in predict.items():
        for title,value in dicts.items():
            if cpd in param['molar_mass']:
                predict[cpd][title]=dFBA.convert_g_kg_in_mmol_g(float(value),molar_mass=param['molar_mass'][cpd])   
    return pd.DataFrame(predict)
   
    
def prediction_pur_culture(df_res,predict,param):
    import matplotlib.pyplot as plt
    df=converted_pur_culture(predict, param)
    Y_output_id=param['Y_output_id']
    concentration=df.drop(['confident_interval'])    
    confident_interval=df.drop(['final_concentration (89h)','init_concentration (g/L)'])    
    concentration['time']=[0,89]
    for cpd in ['EX_lac__L_e', 'EX_ac_e', 'EX_ppa_e', 'EX_pyr_e', 'EX_succ_e']:
        confident_interval[cpd]=dFBA.convert_g_kg_in_mmol_g(confident_interval[cpd],molar_mass=param['molar_mass'][cpd])
        for time in ['final_concentration (89h)','init_concentration (g/L)']:
            concentration.loc[time,cpd]=dFBA.convert_g_kg_in_mmol_g(concentration.loc[time,cpd],molar_mass=param['molar_mass'][cpd])


    error_bar=confident_interval.iloc[0].tolist()
    print(error_bar)
 
    
    for idx,cpd in enumerate(predict.keys()):
        fig,ax= plt.subplots()
        ax.scatter(df_res[-1:,0],concentration[cpd][1], label='experimental (fermenteur)',marker='s',color='r',s=25) 
        ax.plot(df_res[:,0],df_res[:,Y_output_id[cpd]], label='predicted',color='grey') 
        ax.errorbar(df_res[-1:,0], concentration[cpd][1], yerr=error_bar[idx], fmt=',r')
        ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
        ax.set_xlabel("Metabolites")
        ax.set_title("Metabolite concentrations at the final time point")
        ax.legend()
        
        # ax.set_yscale('log')
        fig.savefig('pipeline/metabolomique_pur_culture_'+str(cpd)+'.png',bbox_inches='tight')
 


    

