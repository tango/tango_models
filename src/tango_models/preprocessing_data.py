import cobra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from . import dFBA


def experimental_data(data_time_path,path,simulated_data_pre={}):#,simulated_data_pre_transcrip):
    df_time=pd.read_csv(data_time_path[0],sep='\t')
    interest_time = np.array(df_time['time'])
    interest_time = np.delete(interest_time ,2)
    std,_=get_experimental_data(data_time_path,path)
    

    simulated_data_com=pd.DataFrame(simulated_data_pre)
    simulated_data_com.reset_index(level=0, inplace=True)
    simulated_data_com['lactate_mean'] = simulated_data_com[['EX_lac__L_e', 'EX_lac__D_e']].mean(axis=1)
    std=std.fillna(0)
    simulated_data_com=simulated_data_com.to_numpy()
    
    print(std['lactate_mean'])
    print(std['lactose_mean'])
    print(std['citrate_mean'])
    print(std['acetate_mean'])
    print(std['propanoate_mean'])
    
    fig, ax = plt.subplots()
    ax.scatter(interest_time, std['lactate_mean'], label='lactate experimental',marker='s',color='r',s=5)  
    ax.plot(interest_time, simulated_data_com[:,-1],label='lactate simulated',color='r')
    error_lactate = [0.0,0.16,0.38,0.2,0.2,0.38]
    error_lactate = [dFBA.convert_g_kg_in_mmol_g(i,molar_mass=90.08) for i in error_lactate]
    plt.errorbar(interest_time, std['lactate_mean'],yerr=error_lactate, fmt = 's',color='r')
    ax.legend()
    # ax.set_yscale('log')
    ax.set_xlabel('time [h]')
    ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
    fig.savefig('pipeline/metabolomique_comparaison_lactate.png',bbox_inches='tight')
 

    fig, ax = plt.subplots()
    ax.scatter(interest_time, std['lactose_mean'],label='lactose experimental',marker='s',color='r',s=5) 
    ax.plot(interest_time,simulated_data_com[:,1],label='lactose simulated',color='r')
    error_lactose = [0.0,1.17,0.10,1.07,0.09,0.06]
    error_lactose = [dFBA.convert_g_kg_in_mmol_g(i) for i in error_lactose]
    plt.errorbar(interest_time, std['lactose_mean'],yerr=error_lactose, fmt = 's',color='r')
    ax.legend()
    # ax.set_yscale('log')
    ax.set_xlabel('time [h]')
    ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
    fig.savefig('pipeline/metabolomique_comparaison_lactose.png',bbox_inches='tight')
 

    fig, ax = plt.subplots()
    ax.scatter(interest_time, std['citrate_mean'],label='citrate experimental',marker='s',color='r',s=5) 
    ax.plot(interest_time, simulated_data_com[:,6],label='citrate simulated',color='r')
    error_citrate = [0.0,0.03,0.04,0.01,0.0,0.0]
    error_citrate = [dFBA.convert_g_kg_in_mmol_g(i,molar_mass=192.124) for i in error_citrate]
    plt.errorbar(interest_time, std['citrate_mean'],yerr=error_citrate, fmt = 's',color='r')
    ax.legend()
    # ax.set_yscale('log')
    ax.set_xlabel('time [h]')
    ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
    fig.savefig('pipeline/metabolomique_comparaison_citrate.png',bbox_inches='tight')
 

    fig, ax = plt.subplots()
    ax.scatter(interest_time, std['diacetyle_mean'],label='diacetyl experimental',marker='s',color='r',s=5) 
    ax.plot(interest_time, simulated_data_com[:,4],label='diacetyl simulated',color='r')
    ax.legend()
    # ax.set_yscale('log')
    ax.set_xlabel('time [h]')
    ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
    fig.savefig('pipeline/metabolomique_comparaison_diacetyl.png',bbox_inches='tight')
 

    fig, ax = plt.subplots()
    ax.scatter(interest_time, std['acetate_mean'],label='acetate experimental',marker='s',color='r',s=5) 
    ax.plot(interest_time, simulated_data_com[:,7],label='acetate simulated',color='r') 
    error_actetate = [0.0,0.05,0.08,0.05,0.16,0.05]
    error_actetate = [dFBA.convert_g_kg_in_mmol_g(i,molar_mass=60.052) for i in error_actetate]
    plt.errorbar(interest_time, std['acetate_mean'],yerr=error_actetate, fmt = 's',color='r')
    ax.legend()
    # ax.set_yscale('log')
    ax.set_xlabel('time [h]')
    ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
    fig.savefig('pipeline/metabolomique_comparaison_acetate.png',bbox_inches='tight')
 

    fig, ax = plt.subplots()
    ax.scatter(interest_time, std['propanoate_mean'],label='propionate experimental',marker='s',color='r',s=5)
    ax.plot(interest_time, simulated_data_com[:,8],label='propionate simulated',color='r')
    error_propionate = [0.0,0.0,0.0,0.01,0.24,0.02]
    error_propionate = [dFBA.convert_g_kg_in_mmol_g(i,molar_mass=74.08) for i in error_propionate]
    plt.errorbar(interest_time, std['propanoate_mean'],yerr=error_propionate, fmt = 's',color='r')
    ax.legend()
    # ax.set_yscale('log')
    ax.set_xlabel('time [h]')
    ax.set_ylabel("concentration [mmol.g_{milk}^{-1}] ")
    fig.savefig('pipeline/metabolomique_comparaison_ppa.png',bbox_inches='tight')
 
    
 
 
    ##### add transcriptomic model ####
    
    # simulated_data_pre_transcrip['time']=simulated_data_pre_transcrip.index
    # simulated_data_transcrip=simulated_data_pre_transcrip.loc[simulated_data_pre_transcrip['time'].isin(interest_time)]
    # simulated_data_transcrip['time'] = simulated_data_transcrip['time'].replace(interest_time,interest_value)
    # print(simulated_data_transcrip)
 
    # ax[0].plot(simulated_data_transcrip['time'], simulated_data_transcrip['EX_lac__L_e'],'b',label='l_lactate simulated_R')
    # ax[0].plot(simulated_data_transcrip['time'], simulated_data_transcrip['EX_lac__D_e'],'b',label='d_lactate simulated_R')
    # ax[1].plot(simulated_data_transcrip['time'], simulated_data_transcrip['EX_lcts_e'],'b',label='lactose simulated_R')
    # ax[2].plot(simulated_data_transcrip['time'], simulated_data_transcrip['EX_cit_e'],'b',label='citrate simulated_R')
    # # ax[2].plot(simulated_data_com['time'], simulated_data_com['EX_diact_e'],'g',label='diacetyl simulated')
    # ax[3].plot(simulated_data_transcrip['time'], simulated_data_transcrip['EX_ac_e'], 'b',label='acetate simulated_R')
    # ax[4].plot(simulated_data_transcrip['time'], simulated_data_transcrip['EX_ppa_e'], 'b',label='propionate simulated_R')

 
 
    
    # plt.tight_layout()

    # plt.show()

                

def get_experimental_data(interesting_path_time, DM_path, path, interest_value = ['Linoc','Moul','Demoul','AS','Aff4s','Aff7s']):    

    df=pd.read_excel(path,engine='openpyxl')
    std=df[(df['VarainteProcess']=='STD')]
    std=std[['Stade','Lactate-HPLC-F3','Lactate-HPLC-F1','Propanoate-HPLC-F1', 'Propanoate-HPLC-F3','Lactose-HPLC-F1', 'Lactose-HPLC-F3','Acétate-HPLC-F1','Acétate-HPLC-F3','Diacétyle-F1','Diacétyle-F3','Citrate-HPLC-F1','Citrate-HPLC-F3']]
    std=std.loc[std['Stade'].isin(interest_value)]

    std["Lactose-HPLC-F1"]=dFBA.convert_g_kg_in_mmol_g(std["Lactose-HPLC-F1"])
    std["Lactose-HPLC-F3"]=dFBA.convert_g_kg_in_mmol_g(std["Lactose-HPLC-F3"])

    std["Lactate-HPLC-F1"]=dFBA.convert_g_kg_in_mmol_g(std["Lactate-HPLC-F1"],molar_mass=90.08)
    std["Lactate-HPLC-F3"]=dFBA.convert_g_kg_in_mmol_g(std["Lactate-HPLC-F3"],molar_mass=90.08)

    std["Propanoate-HPLC-F1"]=dFBA.convert_g_kg_in_mmol_g(std["Propanoate-HPLC-F1"],molar_mass=74.08)
    std["Propanoate-HPLC-F3"]=dFBA.convert_g_kg_in_mmol_g(std["Propanoate-HPLC-F3"],molar_mass=74.08)

    std["Acétate-HPLC-F1"]=dFBA.convert_g_kg_in_mmol_g(std["Acétate-HPLC-F1"],molar_mass=60.052)
    std["Acétate-HPLC-F3"]=dFBA.convert_g_kg_in_mmol_g(std["Acétate-HPLC-F3"],molar_mass=60.052)

    std["Diacétyle-F1"]=dFBA.convert_g_kg_in_mmol_g(std["Diacétyle-F1"],molar_mass=86.08)
    std["Diacétyle-F3"]=dFBA.convert_g_kg_in_mmol_g(std["Diacétyle-F3"],molar_mass=86.08)
 
    std["Citrate-HPLC-F1"]=dFBA.convert_g_kg_in_mmol_g(std["Citrate-HPLC-F1"],molar_mass=192.124)
    std["Citrate-HPLC-F3"]=dFBA.convert_g_kg_in_mmol_g(std["Citrate-HPLC-F3"],molar_mass=192.124)
 

    lactate_col=std[['Lactate-HPLC-F3','Lactate-HPLC-F1']]
    lactose_col=std[['Lactose-HPLC-F1','Lactose-HPLC-F3']]
    propio_col=std[['Propanoate-HPLC-F1','Propanoate-HPLC-F3']]
    ac_col=std[['Acétate-HPLC-F1','Acétate-HPLC-F3']]
    diac_col=std[['Acétate-HPLC-F1','Acétate-HPLC-F3']]
    diac_col=std[['Diacétyle-F1','Diacétyle-F3']]
    cit_col=std[['Citrate-HPLC-F1','Citrate-HPLC-F3']]
 
    std["lactate_mean"]=lactate_col.mean(axis=1)
    std["lactose_mean"]=lactose_col.mean(axis=1)
    std["propanoate_mean"]=propio_col.mean(axis=1)
    std["diacetyle_mean"]=diac_col.mean(axis=1)
    std["citrate_mean"]=cit_col.mean(axis=1)
    std["acetate_mean"]=ac_col.mean(axis=1)
    std = std.set_index('Stade',drop=False)
#    std["lactose_mean"] = pd.to_numeric(std["lactose_mean"],downcast="float")
#    std["lactate_mean"] = pd.to_numeric(std["lactate_mean"],downcast="float")
#    std["propanoate_mean"] = pd.to_numeric(std["propanoate_mean"],downcast="float")
#    std["acetate_mean"] = pd.to_numeric(std["acetate_mean"],downcast="float")
#    std["diacetyle_mean"] = pd.to_numeric(std["diacetyle_mean"],downcast="float")
#    std["citrate_mean"] = pd.to_numeric(std["citrate_mean"],downcast="float")
    time=pd.read_csv(interesting_path_time[0],index_col='State',sep='\t')    
    DM=pd.read_csv(DM_path[0],index_col='State',sep='\t')    
    std['time'] =  time.loc[interest_value]
    return std, time, DM


# experimental_data('../Téléchargements/TANGO_MTA_WF_310320.xlsx')
