import cobra


def load(list_sbml_path):
    list_model = [cobra.io.read_sbml_model(model_path) for model_path in list_sbml_path]
    return list_model
